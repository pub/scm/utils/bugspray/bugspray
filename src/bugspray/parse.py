#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2023 by the Linux Foundation

import sys
import argparse
import bugspray
import b4
import re

from typing import Tuple, Dict, List

import email.message
import email.utils

logger = bugspray.logger


def new_bug_notification(bid: int, inre_cid: int, dry_run: bool = False):
    msg = email.message.EmailMessage()
    config = bugspray.get_config()
    bodyvals = {
        'bzname': config['bugzilla'].get('name'),
        'bug_url': config['bugzilla'].get('bugmask', '').format(bug_id=bid),
    }
    bodytpt = bugspray.get_template_by_bid('new_bug_notify', bid)
    body = bodytpt.safe_substitute(bodyvals)
    sigtpt = bugspray.get_template_by_bid('botsig', bid)
    sigvals = {
        'myname': config['bugzilla'].get('name'),
        'appname': bugspray.__APPNAME__,
        'appver': bugspray.__VERSION__,
    }
    body += sigtpt.safe_substitute(sigvals)
    msg.set_payload(body, charset='utf-8')
    bugspray.notify_bug(bid, None, msg, inre_cid=inre_cid, dry_run=dry_run)


def make_bug_desc_from_body(product: str, component: str, body: str, vals: Dict) -> str:
    if 'comment_count' in vals and vals['comment_count']:
        tpt_intro = bugspray.get_template_by_product_component('parse_bug_intro_with_count', product, component)
    else:
        tpt_intro = bugspray.get_template_by_product_component('parse_bug_intro', product, component)
    tpt_outro = bugspray.get_template_by_product_component('parse_bug_outro', product, component)
    desc = ''
    intro = tpt_intro.safe_substitute(vals)
    if intro:
        desc = intro + '\n\n'
    desc += body.strip()
    outro = tpt_outro.safe_substitute(vals)
    if outro:
        desc += '\n\n' + outro
    desc += '\n'

    return desc


def new_bug_from_msg(msg: email.message.EmailMessage, product: str, component: str,
                     dry_run: bool = False) -> Tuple[int, int]:
    msgid, author, subject, body, atts = bugspray.msg_parse_for_bug(msg)
    payload = bugspray.get_newbug_payload_by_product_component(product, component)
    summary = re.sub(r'^\s*(Re|Fwd):\s*', '', subject)
    vals = {
        'author': b4.format_addrs([author]),
        'msgid_link': bugspray.get_msgid_link(msgid),
    }
    desc = make_bug_desc_from_body(product, component, body, vals)

    payload['summary'] = summary
    payload['description'] = desc
    if not dry_run:
        bid, cid = bugspray.bz_add_new_bug(payload)
        logger.debug('new bug bid=%s, cid=%s', bid, cid)
        recipients = bugspray.msg_get_recipients(msg)
        bugspray.db_store_recipients(bid, recipients)
        if atts:
            bugspray.bz_add_atts_to_bug(bid, atts)
    else:
        logger.info('--- DRY RUN ---')
        logger.info('Would have created a new bug in %s/%s:', product, component)
        logger.info('Summary: %s', payload['summary'])
        logger.info('Description:')
        logger.info(payload['description'])
        bid = cid = None

    return bid, cid


def new_comment_from_msg(bid: int, cid: int, msg: email.message.EmailMessage, dry_run: bool = False) -> int:
    msgid, author, subject, body, atts = bugspray.msg_parse_for_bug(msg)
    vals = {
        'author': b4.format_addrs([author]),
        'msgid_link': bugspray.get_msgid_link(msgid),
    }
    if cid:
        try:
            vals['comment_count'] = bugspray.bz_get_count_by_bid_cid(bid, cid)
        except LookupError:
            pass
    product, component = bugspray.bz_get_product_component_by_bid(bid)

    desc = make_bug_desc_from_body(product, component, body, vals)

    if not dry_run:
        cid = bugspray.bz_add_new_comment(bid, desc)
        recipients = bugspray.msg_get_recipients(msg)
        bugspray.db_store_recipients(bid, recipients)
        if atts:
            bugspray.bz_add_atts_to_bug(bid, atts)
    else:
        logger.info('--- DRY RUN ---')
        logger.info('Would have added this comment to %s', bid)
        logger.info(desc)
        cid = None
    return cid


def get_tag_info(msg: email.message.EmailMessage, regexes: List[str]) -> Tuple[str, str]:
    payload = bugspray.msg_get_payload(msg)
    fromaddr = bugspray.msg_get_author(msg)[1]

    config = bugspray.get_config()
    regex = None
    tdata = None
    for regex in regexes:
        matches = re.search(regex, payload, flags=re.I | re.M)
        if matches:
            tdata = matches.groups()[0]
            break

    if not tdata:
        raise LookupError('Nothing matched tag regexes')

    # handle our special alias first
    if tdata == 'me':
        logger.debug('me=%s', fromaddr)
        tdata = fromaddr
    else:
        try:
            aliases = config['bugzilla'].get('aliases', dict())
            tdata = aliases[tdata.lower()]
            logger.debug('Resolved alias to %s', tdata)
        except KeyError:
            pass

    if '@' in tdata:
        # Does this user exist?
        try:
            bugspray.bz_get_user(tdata)
            logger.debug('found user=%s (matched regex: %s)', tdata, regex)
            # First match wins
            return 'user', tdata
        except LookupError:
            logger.info('Unable to resolve %s: no such user', tdata)
            raise LookupError('Unable to resolve %s: no such user' % tdata)

    if '/' in tdata:
        # Try to find this as a defined product/component
        chunks = tdata.split('/')
        if len(chunks) == 2:
            product, component = [x.strip() for x in chunks]
            if product in config['products'] and component in config['products'][product]['components']:
                logger.debug('found product=%s, component=%s (matched regex: %s)', product, component, regex)
                return 'component', f'{product}/{component}'

    # finally, try to find this subsystem
    mdata = bugspray.get_maintainer_data()
    if tdata in mdata:
        return 'subsystem', tdata

    raise LookupError('Could not map tag %s to anything useful' % tdata)


def process_rfc2822(msg: email.message.EmailMessage, product: str, component: str,
                    dry_run: bool = False) -> None:
    # Ignore any messages that have an X-Bugzilla-Product header,
    # so we don't get into any loops
    if msg.get('x-bugzilla-product'):
        logger.debug('Skipping bugzilla-originating message')
        return

    cconf = bugspray.get_component_config(product, component)
    # Get the message-id
    msgid = b4.LoreMessage.get_clean_msgid(msg)
    try:
        # If we have this exact msgid, then it's a dupe
        bid, cid = bugspray.db_get_bid_cid_by_msgid(msgid)
        logger.info('Already recorded as bid=%s, cid=%s', bid, cid)
        return
    except LookupError:
        pass

    # Walk through references and in-reply-to and see if we know any of them
    bid = cid = None
    try:
        bid, cid = bugspray.msg_get_inre_bid_cid(msg)
    except LookupError:
        pass

    if bid:
        bdata = bugspray.bz_get_bug(bid)
        if not bdata['is_open']:
            logger.info('Bug %s is closed, not adding comments', bid)
            sys.exit(0)

        cid = new_comment_from_msg(bid, cid, msg, dry_run=dry_run)
        if not dry_run:
            bugspray.db_store_msgid_bid_cid(msgid, bid, cid)
    else:
        bid, cid = new_bug_from_msg(msg, product, component, dry_run=dry_run)
        if not dry_run:
            bugspray.db_store_msgid_bid_cid(msgid, bid, cid)
            if cconf.get('new_bug_send_notification'):
                new_bug_notification(bid, cid, dry_run=dry_run)

    # Do we have any assign triggers?
    pst = cconf.get('pi_search')
    if not pst:
        return

    pconf = bugspray.get_pi_search(pst)
    tag_res = pconf.get('tag_regexes')
    if not tag_res:
        return

    try:
        ttype, tdata = get_tag_info(msg, tag_res)
    except LookupError:
        return

    if ttype == 'component':
        product, component = tdata.split('/')

    # Is this person allowed to tag?
    author = bugspray.msg_get_author(msg)
    fromaddr = author[1]
    if not bugspray.bz_check_user_allowed(fromaddr, product, component):
        logger.info('%s is not allowed to tag, ignoring: %s', fromaddr, tdata)
        return

    if dry_run:
        logger.debug('---DRY RUN---')
        logger.debug('Would have processed tag: bid=%s, type=%s, data=%s', bid, ttype, tdata)
        return

    if ttype == 'user':
        bugspray.bz_assign_bug(bid, tdata)
        return
    if ttype == 'component':
        bugspray.bz_set_bug_product_component(bid, product, component)
        return
    if ttype == 'subsystem':
        bugspray.bz_set_bug_subsystem(bid, tdata)
        return

    logger.info('Warning: unknown tag type: %s', ttype)


def main(cmdargs: argparse.Namespace) -> None:
    msg = bugspray.get_msg_from_stdin()
    product = cmdargs.product
    component = cmdargs.component
    if not (product and component):
        recipients = bugspray.msg_get_recipients(msg)
        try:
            product, component = bugspray.get_product_component_by_recipients(recipients)
        except LookupError as ex:
            # TODO: fail properly here
            logger.info(str(ex))
            sys.exit(1)
    process_rfc2822(msg, product, component, dry_run=cmdargs.dry_run)
